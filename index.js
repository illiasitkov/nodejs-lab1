const express = require('express');
const server = express();
const fileRouter = require('./routers/fileRouter');
const cors = require('cors');

const errorMessage = (message) => ({message});

const errorHandler = (err,req,res,next) => {
    if (err.fileError) {
        res.status(400);
        res.json(errorMessage(err.message));
    } else {
        res.status(500);
        res.json(errorMessage('Server error'));
    }
    next(err);
};

server.use(cors());
server.use(express.static(__dirname));
server.use('/api/files',fileRouter);
server.use(errorHandler);

server.listen(8080);
