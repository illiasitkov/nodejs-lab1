const express = require('express');
const {fileLogger, consoleLogger} = require('../utils/logging');

const fileRouter = express.Router();

fileRouter.use(fileLogger);
fileRouter.use(consoleLogger);
fileRouter.use(express.json());

const FileService = require('./../services/fileService');

const getAllFilesResponseBody = (files) => ({
    message: 'Success',
    files
});

const getFileResponseBody = (fileInfoObject) => ({
    message: 'Success',
    ...fileInfoObject
});

fileRouter.route('/')
    .get((req,res) => {
        res.status(200);
        res.json(getAllFilesResponseBody(FileService.getAllFileNames()));
    })
    .post((req,res) => {
        FileService.createFile(req.body);
        res.status(200);
        res.json({message: 'File created successfully'});
    });


fileRouter.route('/:filename')
    .get((req,res) => {
        res.status(200);
        res.json(getFileResponseBody(FileService.getFileInfoByName(req.params.filename)));
    })
    .put((req,res) => {
        FileService.updateFile({filename: req.params.filename, content: req.body?.content});
        res.status(200);
        res.json({message: 'File updated successfully'});
    })
    .delete((req,res) => {
        FileService.deleteFile(req.params.filename);
        res.status(200);
        res.json({message: 'File deleted successfully'});
    });


module.exports = fileRouter;
