const fs = require('fs');
const path = require("path");

const dir = path.join(__dirname, path.normalize('../files'));

const ACCEPTABLE_EXTENSIONS = ['.log','.txt','.json','.yaml','.xml','.js'];

const filePath = (name) => {
    return path.join(dir,name);
}

const fileInfoObject = (filename, content, extension, uploadedDate) => ({
    filename,content,extension,uploadedDate
});

const getAllFileNames = () => {
    if (fs.existsSync(dir)) {
        return fs.readdirSync(dir);
    }
    throw new Error();
}

const getFileInfoByName = (name) => {
    if (fs.existsSync(filePath(name))) {
        const content = ''+fs.readFileSync(filePath(name),{flag: 'r'});
        const extension = path.extname(name).slice(1);
        const uploadedDate = fs.statSync(filePath(name)).birthtime;
        return fileInfoObject(name,content,extension,uploadedDate);
    }
    throw {fileError: true, message: `No file with \'${name}\' filename found`};
}

const checkFileObject = (fileObj) => {
    if (!fileObj.filename) {
        throw {fileError: true, message: 'Please specify \'filename\' parameter'};
    }
    if (!fileObj.content) {
        throw {fileError: true, message: 'Please specify \'content\' parameter'};
    }
}

const createFile = (fileObj) => {
    checkFileObject(fileObj);
    if (fs.existsSync(filePath(fileObj.filename))) {
        throw {fileError: true, message: `File with name \'${fileObj.filename}\' already exists`};
    }
    const extension = path.extname(fileObj.filename);
    if (!ACCEPTABLE_EXTENSIONS.includes(extension)) {
        throw {fileError: true, message: `File extension \'${extension}\' is not supported`};
    }
    if (!fs.existsSync(dir)) {
        fs.mkdirSync(dir);
    }

    fs.writeFileSync(filePath(fileObj.filename),fileObj.content);
};

const updateFile = (fileObj) => {
    checkFileObject(fileObj);
    if (!fs.existsSync(filePath(fileObj.filename))) {
        throw {fileError: true, message: `File with name \'${fileObj.filename}\' does not exist`};
    }
    fs.writeFileSync(filePath(fileObj.filename),fileObj.content);
}

const deleteFile = (filename) => {
    if (!fs.existsSync(filePath(filename))) {
        throw {fileError: true, message: `File with name \'${filename}\' does not exist`};
    }
    fs.unlinkSync(filePath(filename));
}

const FileService = {getAllFileNames,getFileInfoByName,createFile,updateFile,deleteFile};

module.exports = FileService;
